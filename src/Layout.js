import React, {Component} from 'react';

import Nav from './Page/Nav';
import MainPage from './Page/MainPage';

import AboutWisebirds from './Page/AboutWisebirds';
import Services from './Page/Services';
import Adwitt from './Page/Adwitt'
import Resources from './Page/Resources';
import ContactUs from './Page/ContactUs';

import './Layout.scss';

class Layout extends Component {
  render() {
    return (
      <div>
        <header className="header">
          <Nav/>
        </header>
        <div id="fullpage">
          <div className="section main__section">
            <MainPage/>
          </div>
          <div className="section">
            <AboutWisebirds/>
          </div>
          <div className="section">
            <Services/>
          </div>
          <div className="section">
            <Adwitt/>
          </div>
          <div className="section">
            <Resources/>
          </div>
          <div className="section">
            <ContactUs/>
          </div>
        </div>
      </div>
    );
  }
}
export default Layout;
