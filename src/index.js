import $ from 'jquery';
import 'fullpage.js';

import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './Layout';

import './index.scss';

ReactDOM.render(
  <Layout />,
  document.getElementById('root')
);

// FullPage Option
var options = {
  // paddingTop: 72,
  anchors: ['main', 'aboutWisebirds', 'services', 'adwitt', 'resources', 'contactUs'],
  loopHorizontal: false
};

$('#fullpage').fullpage(options);