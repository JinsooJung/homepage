import React, {Component} from 'react';
// import reactHtmlParser from 'react-html-parser';
import Truncate from 'react-truncate';
import './Resources.scss';


/*
  JSON구조짜기
  type: "Blog","Report","Case Study"
    Blog = WISE BLOG
    Report = DOWNLOAD<br/>THE REPORT
    Case Study = WISE<br/>CASE STUDY

  type에 따라 팝업 구조가 달라짐

  Blog.
    타이틀,
    실제 활용 사례,
    작성일,
    요약
  Report.
    이메일 전송 폼.
  Case Study.
    성공사례,
    타이틀,
    배경,
    목표
    해결방안,
    결과
 */


var blogJson = {
  "case": "Blog"
  // ,"title": "Facebook & Instragram Flexible Targeting"
  // ,"useCases": "[플랙서블 타겟팅 실제 활용 사례]"
  // ,"date": "2016-07-04"
  // ,"summary": "국가는 지역간의 균형있는 발전을 위하여 지역경제를 육성할 의무를 진다. 대통령은 내란 또는 외환의 죄를 범한 경우를 제외하고는 재직중 형사상의 소추를 받지 아니한다."
};

export default class Resources extends Component {
  render(){
    return (
      <div className="resources">

        <div className="slide resources-0">
          <h3 className="title">RESOURCES</h3>
          <ul className="resources__list">
            <li className="resources__list-item">
              <a href="#">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAAwCAYAAABOiRchAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTMyIDc5LjE1OTI4NCwgMjAxNi8wNC8xOS0xMzoxMzo0MCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTNCMTdCNzE3RkNGMTFFNjhEMTZGMDBGQTJFRUM2QzUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTNCMTdCNzI3RkNGMTFFNjhEMTZGMDBGQTJFRUM2QzUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxM0E3RjFGMjdGQ0YxMUU2OEQxNkYwMEZBMkVFQzZDNSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxM0IxN0I3MDdGQ0YxMUU2OEQxNkYwMEZBMkVFQzZDNSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PqlcyUEAAATgSURBVHja3JtraBRXFMfvbqLG2gZjq62N1g8WRbAPFW2r1lq1X3whaAsqtF/aUmpURKNpoQhamqp9YNWKUsRCRUVaUaHoBzVEjUorfcRHxYD4joqmTUSTzWP6P+5/yGU6szO7mdm76R9+zGTnseOZe84959w1ZlmW0jQevAmGg0JgkbAV531vgb1gE3igDClGIzwDtoGxhp7jEngZ1JoyQh9sz4AnHMcSfGNRSazfRfv7mKmXkA+WaQaQN1EKToCGLBihP9gFBoAxoB+4mnUrYCRUW0ndA8XiHlmm1GrXDAPf//BNd6U9boNrBkZjQtvPM+EOYoRmzTXiBp6hm7bfYsoIplXrmCWMBEY/fQgGgrYUhpRg9rXj89HMN/pxFnC7vhG8qv39CTjnGB2+M5zmzr+Cg5kExtMMSldA3CVw3LT8ldDOl+C62zKnrZkERj/dDHDOdW4LwB9ghkH3egcsDtsd5oIi0OpxXCJ6Pfcl/X2c+/fBOnAANGnDNop8I063Wsnn+QB8GaY7pMM/vJdsnzUw5x/h9zeE7Q5B1VWb8/8ENQZc4YrpKTKuVZwxQ/HA/t7WsGPCRjDMY4qLc2rc6Si7TeUf74M5jGGhGkEi/VMpjj9PI+SCGri9G7Y7XPA5flt1cgUZCW+BHh5+lmfICD1BXQbXLQTfZGKEWvX/0Ao3A+RKAZUNbQDLc7mKjFo7wIKOxoTtYJRy7zrHmKpuzVED7Gct0dZRI0wAfVIcHxTBw7/gk3DdD3CP42Cmo3OVsREqVbILnPDoCkWRHk8CX6Q4vsrn+tNgSkBjBZ4i8z1aX12CWDoDSQUoHfCyDK69SBdtCjNZkljQrKXFOokI/flj8F2a19yiAX5gQMwLagT7xBa/AJJlWewL/BTw/Ho2UzZwhE4DW4IUc3rl1yMHo7tkqbPBIZ/zZB1zPlgLumufvx2kuSK+Lu2wIaA3u0jbcswQCRZx0kAd6WGoEvAZ6OVyfBH4G7yoko1fp/ZJZ2Wi1qR8AF7pQGfnGu9TFUHXqDc4x/t/rj3zPFAToAG7GFxwfFYJHsmnhX8Gk1WyUVoFNoO/+BZkeFWrZK/QTa+BEdwvjHBESKH2Bud/Wx+Bd1VyScBPq+kyEnCL6QHTHk6jtHI38LuPJae7vJ3xHudWRdhHHAyWgHJQkWY7XpYG3gMnZTXevqd+c1mm/5Yneqmvdn4haDRghNCJOX6pojhUpoK+nDZlCI7jMfkdw1DuH1XJ5XTRMebpZZxlZMiO7iwVlpsR3HRDa7GVsI31vZbH29PrdRqvkrGiUyhoKT1Z21+vGUA0XZtu7V+eFBj69/zIvKc+CiP8Bpa6fL5GtS+Atqj2Zf5hbIFlW8XcNqeXm6YXRA5qwe+Uy/G12vGKLAe4nsxzROc7GhhTqYD+LiX06+q/re1HwR3V/uuXUyyJz6fZxcpjKlzt08coYrUo26+YFdqldllUIyEI40JcZj/g8R3lPrlAUTrPHNXQfAn8EpIhyh33npri3DtgTBh5QpiaBZ6jmwQt0yWoPcap2Ja4XgWn6ata+b8bnKV7XmYPIf11kBzO5Modb/hpsEf77EiUGWMu6SQ73YrlsD3t3gNPBu0hhpUxmlIRs9ACl8q1MtsZoynVaRmprU/DNEBnGAm2lrMXcFgl/ytCqPpXgAEAjHE8Bc3meYsAAAAASUVORK5CYII=" alt="BLOG"/>
                <span>BLOG</span>
              </a>
            </li>
            <li className="resources__list-item">
              <a href="#">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAA2CAYAAAC8yXv8AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTMyIDc5LjE1OTI4NCwgMjAxNi8wNC8xOS0xMzoxMzo0MCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTNBN0YxRUM3RkNGMTFFNjhEMTZGMDBGQTJFRUM2QzUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTNBN0YxRUQ3RkNGMTFFNjhEMTZGMDBGQTJFRUM2QzUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxM0E3RjFFQTdGQ0YxMUU2OEQxNkYwMEZBMkVFQzZDNSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxM0E3RjFFQjdGQ0YxMUU2OEQxNkYwMEZBMkVFQzZDNSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pl23W4YAAAPySURBVHja7JpZSFRRGMfvvTNYYla2QLsvEmRGqwYRpFBRFNmCtEAP0UrRShKULWKLUVBgD0ZUEFRGVPRQ9FBUpg899BAtIEGLGtmmE1Gpo97+H343Toe5986dOTNdwj/8mDt3O59nvvN95ztH3TRNTaGWg80gExigCZwDFcpaIIMVMcu01xZV7egKe7gBjHC4Phh8ibcRQ5GxFS7Gkm6paC+WHu4DUsFAkAdWgplRPvscnADV4CPoAL9UGtwXpIAu0AwmgHsgDfSKs7O+gzDYCK6ADBDgc9+8uEQ2OAgesl8Sr/jaSDBAgbGkdH7XJP5ew201gkfgGJjqFiXKHEY6XZ9nqlcJv7vZ5nqlaKPYw1WgxKVX2jT1yuDPNJvr60Gt9SXIn3vAUulG8tXbIMQ+TPqcAINf8OcS0I9dZQ4oFO6ZBi6BFTTo+uOgRbjYChaBOzYNZHFvqAjgFCVe2lybDm6AQcK5HPKLXZLPFCnMfvEyWbLtDPlwvvAXvAFXNf/oCUcrSxMNDi2W7mv+U7U4QA3JFzt8aLBon24IEUDl3EKlgsJxl2ygrvlcqns0xD+hG8dVGZyMHr4AilUZHE6wsZSM1sSTdIwImSdReszpN6xqBEbrEstA7wjn6x2eqQPzwU+VISNaUQ9djPDr3LS5/z2YraKeizVKXOMqIRpR5VAJylUN6GCMz50GQ8F+h3uoVjsEDnM7n8C2f2Uw6QAYwhNsWZQ9Sxmrja3gAzjLhayst8lIHJvAdZs/ppira1Hl/MxTnhlarE5WpuukKkCaUVEWW8vLAJG0FxwRwhstZe1LhkuIdd4CsIOr67lcXdspwAaWcsW8zksiUWGwFQ2sAVisJVB+nE72GNxjcCISxyqQy2HMTRQFLvP6mKV8XqjpdOi4Rs6KSgzeDsZ56IgWyWAKdRuieE6Zwc/AGKlg1Vx6S1QD967d8zq3ocwlKJ3u9GCwvL57nlN4l8Mz7SoNDjGx6gfTE9b+m7A2TOve7zAdBs1X7e/1Y5paZvKx2+RG5wn/O1UGn9K614yddJLDnyWaidV46DxaJx6ryiWyorhnlPQ93eOvnR2LS5gOAT3H5V21EXqszIPBTQ7u8uc4yKlUrMUiqSqG8VHvpZJwUKtY4cjLrSk+DAzialSrIS1wFPjQYNGmNkMayaNBkY+Mpe0ucR+7jnZqMqSdmjAo9MEOUgEISbblWpvju3mVRtRd7v3XHD2SkRWp9B8OZmjdi4eiaM9usbibTycW+jQj027peCtKWKJMdtSHxtKW7RSrWon0/xI0UafVyTyeQ6Tyze3sGjp/BjgMmlrsK+oGh60wv1fntpp5Qk+rQg/EB34LMADLfiR65KnfTgAAAABJRU5ErkJggg==" alt="REPORT"/>
                <span>REPORT</span>
              </a>
            </li>
            <li className="resources__list-item">
              <a href="#">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADsAAABBCAYAAABvnNwUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTMyIDc5LjE1OTI4NCwgMjAxNi8wNC8xOS0xMzoxMzo0MCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTNBMzJEMjc3RkNGMTFFNjhEMTZGMDBGQTJFRUM2QzUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTNBMzJEMjg3RkNGMTFFNjhEMTZGMDBGQTJFRUM2QzUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxM0EzMkQyNTdGQ0YxMUU2OEQxNkYwMEZBMkVFQzZDNSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxM0EzMkQyNjdGQ0YxMUU2OEQxNkYwMEZBMkVFQzZDNSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pn2INJIAAAhkSURBVHjazJsJcE1XGMeT2IJYilGqJBjb1FY7KdpSlKFSTJVKLbHWMrTUFGPasVSr2o4l1qKitI1Uw5CiSkWji1qLlsZa+5YWEcLr/7T/zBzHOffe9959L/lmfkPuveee893zne9855zvhXo8npA8IHVADxALToHu4KLblYTmorLFQSfwEnhBufcKWOl2hflzQcmG7MWBoJThmc6BUDZE9GwQKAPiwG7Pw3IfJIK+yvVhLOdaOwKtZCswC9zQKPk7mAGqSM8Lhe9Jz2SCQ2AuKJsXlS0PRoP9GgXvgHXgOU25SmAwyPbo5QDIl1eU7Qw+B1mGho4CFTTlOoLVSo/myFlF+RK5qWxl8CZNUpVMjsVnNOVKgdfAPk25m2AtqAvaSR/hmmQBPUHNYCnbnQ3SiXBCg0A5TbkWYDHI0JT7GQxRxmZ96f51sFlS/i4/WkCUbUqHclTT0AsgHkRryhUDQ8FOTbnLdD5PGeoc5bGWxm4r2x+kGCpLBQNAUU25GmAOP4QqP/C9ujFYgNPUFo+9rHRL2SbgmMFpfMSeVsvkB7Fgk0W5Zob6WoOPwRkL5RaAZOWa6IixYCRo5Iuy4UqPXAEb2RuFNc/XBlPBcU3AIMr1M5SrQFPdbqGgmMImgqpSuWMWz/fyVtkqygt6WjiqrzQVisa8B6IM5drSUd0xNFhMN8s187H4qG/Z9L7H4BxDrKYUWdJAHRAGImk2e5RnhHf8GvQBoYagQQQb2ywaKTz56wxM5LIxIMmi3CVwVfr7aW+UjTK89BRNUxYRzk0Bjxve1R4s05STG/oJaK6UqwYmgHMWSu5kDB3BwCVHWrqhrDrdtDGUF+NrPPjDpqG9QSFNRJViUU7Mt4tAA6WcbNrRvip7WRPp/GMYjyJkXGHR0PNgocZrlqOJH7ExcTFeH9HUK4bNSTeUPc1rtaRxulV5foyNh/yOzqygZrpJsnBUtziXRtvMoY6UdbJ4L8F/D4MToD64xWv5QApoqyn3F4gHyeCAskMxgjsUdQx17gXzwVpwIZg7FfdAQXAHFFXKPaEoKvaN9oC5YAPL5kgDMBzEgJKaesT7N4IPwfZAbJH4uy0Tofw9FCRJf4dz+6UnaGF4xyH2YhKtIWDir7LZyt9T2bvC3PuAUeBRTbn7YB0tYLMX9RWRhlCub7jVBDuoTJjm/nGwAKwGJ714b2UwHSQSnyQsQBYjv/cuSOCOYVUww6GioaAx+Amkc5wn5qYZyyLMKw1UBxWl613prJyK8PAdaQHlJec1Ii/tGxegZxa9miE5r98cli8M+oNZ9P6yzARH85qyhUAWTTdHyjgw2xrgiOHeKTo+V8eWLFd9fF9BH31BlMW90f54YCfKRgX5SKSE4fpGZd52VdlaYBr4PsjK6iKqLBunVNji3k2rMdsSvAG65NKJnq5nlzMc/VO5XoHBynV2jOMPIXp2LHsytxSNBuWUaye4SEiUjjPrghUMTPoy+jLJKvCsTtl+yrXTYCK4HEAFw3jgvAukao4uxWqnOT18EuPrNTy3FdfGc3ozSST4FgxRK72hefg6x4zbUoQrn2PgS9BUY3abQC+ljWKhMA8c5Ada6rC+ePnZ/JpgXkQ/cxjf/neES9yQZTyIVhcFRfj/2+AcaKexhFkMOXfYtEeNy4XJVxMfMMyL+DYnlPNHXuVqR5YMyUEtpamqMgWUButtzFdIJpitDEPhFxJVZbfSOai5D735/yt+KpvJsZqsUTadKQj5pFg7jiY+ySbQ8UgWGs61c7jyTJUwjRcT2y4/KoG5WLWMoXPwV0RQ/yL4TFK2JHu8CUPL9qAYWELTtpuj46R1cz5+NHVj4boaG4t5bTFoxor6S/c+0CzaQ31UWGzXxPI9kYyLo/ih9zv0EfVocQMNQcle7l+1Ny0EZGc1gPtJsy2Ckdt+9PA9Oo9obshlOCgTRicldkG6GZ4RDuxTdlobK2VVmUNXn8CVSY60IvvYaF8dl4fzrJ1U5D7WUO5ahGicnJiHF7G98mrKqyXeL6A2v9bL0ry4nZFXtgte2iStuerpZGjrIc6lImfqmsF6vF7PZnOi/xW8r/Sw8eU+ioimYtiLDQ3PJHOaWuvP4v2azfMzaSbCZMoatmZCfPTawuEMo7kW19w/yx5cyAjMpxhVDtXqOyiTyiB9l+beZP570Yv6xclACr3nII2iYl9rJMfqOF8V/d9DPHzOmsyTdyd5CvM05zNpyrlLI8Nh9xTlOTWtaAVTHfxJXRosH5CHMMfhb6WydEPOhI7eNkeb8sFyV8NJfY6IU7zhppNzHxPRHlA256Q9TVP5KIcvFSk6hzXlb4MOTPA6aJP10sHltEJhPQlSHSfUBxZrGjLf4cvFofJ6pWyWhYIXwTu+ZKrZEM18DDU98Jbu4SGGcRjpsLIJNma9helDES4qWIxZdTsNdYphOsjKLNM1BWIcVt6N6QDy6f1CHj672YtPgpk80dfJHiaklLVL+grXJFgJmeSwIY/RW0/ml3dTyR5gg4X1JDI3w+t0vumal60xJHEFEjGMxoG9BgVFwtm7TO70K1Gzi2Z6OsrcqGBko8dbpBZtY/6kq1mpFQzTU1wAFIygArsNCooU3iWgXqDzjXWpPzNcUrImp6NLBiUP0WeUDmYmuW562uHHrzVEFtw3Fg4nxfC7gqClzTdiWq2amve8w/JiOnjbkKztYRqf8OTV88pPXUoacotH2syNy/hbAI8hfT7Wi8VI0H/9MU3T6C+k+2FcgaQaFLzL9PmWgfTsbv4WL4ZbsYWUza80LsgrGbZ8ErgZcCbgR2guf71qmvWxTpK53AtmUBKwX1ku5TapLOd55roy5MFcxqBJIH9SOow7g1e5b7TKrdwIX+VfAQYAA6GBtdr8ztcAAAAASUVORK5CYII=" alt="CASE STUDY"/>
                <span>CASE<br/>STUDY</span>
              </a>
            </li>
          </ul>
        </div>

        <div className="slide resources-1 resources__blog">
          <h4 className="subtitle"><span>WISE</span> <strong>BLOG</strong></h4>
          <p className="explain">지금 이순간 가장 TRENDY하고 영향력 있는 매체들의 광고 인사이트와 팁!</p>
          {/*Search Bar - Start*/}
          <SearchPanel/>
          {/*Search Bar - End*/}

          {/* BLOG List - Start*/}
          <div className="blog__list">
            <div className="blog__list-item">
              <div className="blog__list--thumb">
                <img src="http://placekitten.com/400/400" />
              </div>

              <div className="blog__list--text">
                <p className="blog__list--text-title"><strong>Lead Ad Update</strong></p>
                <p className="blog__list--text-subtitle">페이스북 인스타그램 광고 효율</p>
                <hr/>
                <p className="blog__list--text-desc">
                  <Truncate lines={2}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto consectetur dolor eaque eius enim explicabo harum impedit itaque libero, necessitatibus possimus quo repellat saepe sint soluta tempora, temporibus voluptatem.</Truncate>
                </p>
              </div>
            </div>
            <div className="blog__list-item">
              <div className="blog__list--thumb">
                <img src="http://placekitten.com/400/400" />
              </div>

              <div className="blog__list--text">
                <p className="blog__list--text-title"><strong>Lead Ad Update</strong></p>
                <p className="blog__list--text-subtitle">페이스북 인스타그램 광고 효율</p>
                <hr/>
                <p className="blog__list--text-desc">
                  <Truncate lines={2}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto consectetur dolor eaque eius enim explicabo harum impedit itaque libero, necessitatibus possimus quo repellat saepe sint soluta tempora, temporibus voluptatem.</Truncate>
                </p>
              </div>
            </div>
            <div className="blog__list-item">
              <div className="blog__list--thumb">
                <img src="http://placekitten.com/400/400" />
              </div>

              <div className="blog__list--text">
                <p className="blog__list--text-title"><strong>Lead Ad Update</strong></p>
                <p className="blog__list--text-subtitle">페이스북 인스타그램 광고 효율</p>
                <hr/>
                <p className="blog__list--text-desc">
                  <Truncate lines={2}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto consectetur dolor eaque eius enim explicabo harum impedit itaque libero, necessitatibus possimus quo repellat saepe sint soluta tempora, temporibus voluptatem.</Truncate>
                </p>
              </div>
            </div>
            <div className="blog__list-item">
              <div className="blog__list--thumb">
                <img src="http://placekitten.com/400/400" />
              </div>

              <div className="blog__list--text">
                <p className="blog__list--text-title"><strong>Lead Ad Update</strong></p>
                <p className="blog__list--text-subtitle">페이스북 인스타그램 광고 효율</p>
                <hr/>
                <p className="blog__list--text-desc">
                  <Truncate lines={2}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto consectetur dolor eaque eius enim explicabo harum impedit itaque libero, necessitatibus possimus quo repellat saepe sint soluta tempora, temporibus voluptatem.</Truncate>
                </p>
              </div>
            </div>
          </div>
          {/* BLOG List - End*/}

        </div>

        <div className="slide resources-2 resources__report">
          <div className="resources__report-wrapper">
            <div className="resources__report--left-side">
              <h4 className="subtitle">
                <span>WISE</span>
                <strong>REPORT</strong>
              </h4>
              <p className="explain">와이즈버즈는 DATA를 연구하고 정기적인 인사이트를 간행합니다.</p>
              <SearchPanel/>
            </div>
            <div className="resources__report--right-side">
              <div className="report__list">
                <div className="report__list-item">
                  <div className="report__list--thumb">
                    <div className="images">
                      <img src="http://placekitten.com/255/158" />
                      <img src="http://placekitten.com/255/158" />
                      <img src="http://placekitten.com/255/158" />
                    </div>
                    <div className="buttons">
                      <button type="button">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAQAAACR313BAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTMyIDc5LjE1OTI4NCwgMjAxNi8wNC8xOS0xMzoxMzo0MCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RTUwQTEyNTI4M0M3MTFFNjkzQUJBRjc4M0ZCQjdENDgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RTUwQTEyNTM4M0M3MTFFNjkzQUJBRjc4M0ZCQjdENDgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpFNTBBMTI1MDgzQzcxMUU2OTNBQkFGNzgzRkJCN0Q0OCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpFNTBBMTI1MTgzQzcxMUU2OTNBQkFGNzgzRkJCN0Q0OCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Po1HYCAAAAC/SURBVBgZbcE/K4QBAAfg38ly30AUPoZFvUUpA4NuYLciu8UXoG6025RB2ZWSZDORxNGl/AnDdYZH6XrvrrvniYioWHbo3reGU+uqIhIR4870ezAjEjHt0aCWOYkRF4Z7NxY1PV5tayjtx5HSrRWFHaWXeNJxYEFhy5euaPl3o1BY09YrnnXsmVfY9asrjpWuLSrUlZqxqsedDR9K9Rh1ZbhPExGTmga1LUlETLnUr2FWJCKioubEG36c21QVkT+H9YSrBHtCcQAAAABJRU5ErkJggg==" alt="Prev"/>
                      </button>
                      <button type="button">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAQAAACR313BAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTMyIDc5LjE1OTI4NCwgMjAxNi8wNC8xOS0xMzoxMzo0MCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RTUwRUQ2RTg4M0M3MTFFNjkzQUJBRjc4M0ZCQjdENDgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RTUwRUQ2RTk4M0M3MTFFNjkzQUJBRjc4M0ZCQjdENDgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpFNTBBMTI1ODgzQzcxMUU2OTNBQkFGNzgzRkJCN0Q0OCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpFNTBBMTI1OTgzQzcxMUU2OTNBQkFGNzgzRkJCN0Q0OCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pt+h2l4AAAC9SURBVBgZbcExK4QBAAbg92K53cRw5UfY1JfcKMoiZuuVxcQ/MCjL/QK/gG6VRcmkGHXl6CTdgOSWx/L5uq+754mIaNrXM/DpyblNDRGJiBV9ddcWRSLW/Jj2bFliwbvZbjTiRGnkQF/NdrwonSpsuDfhIiq/jhXauirDmDC2p1B48C8q3w4V2roqbzFUOlLY8mhCL86UBjpe1ezGkpHZ7sxHrBubNtSSiFg1UHerJRIR0dRx5QsfLu2YE5E/0l2EsQwXa04AAAAASUVORK5CYII=" alt="Next"/>
                      </button>
                    </div>
                  </div>
                  <div className="report__list--text">
                    <p className="report__list--text-date">[2016.01.27]</p>
                    <p className="report__list--text-title"><strong>2015 Social Ads at a glance</strong></p>
                    <p className="report__list--text-desc">2015년 Social 광고 트렌드를 한 눈에 볼 수 있는 자료입니다 2015년 광고 상품 트렌드 및 관련 사례를 담았습니다.</p>
                    <a href="" download="">DOWNLOAD THE REPORT</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="slide resources-3 resources__case-study">
          <h4 className="subtitle">
            <span>WISE</span> <strong>CASE STUDY</strong>
          </h4>
          <p className="explain">가장 많은 성공사례 FACEBOOK 공식 BUSINESS 페이지 등재,전 세계 최초 INSTAGRAM R&amp;F 캠페인 집행 및 성공 사례 등재 등<br/>수 많은 광고주들이 와이즈버즈와 함께 SUCCESS CASES를 만들어나가고 있습니다.</p>
          <SearchPanel/>
          <div className="case-study__list--wrapper">
            <div className="case-study__list">
              <div className="case-study__list-item">
                <div className="case-study__list-item--thumb">
                  <img src="http://placekitten.com/255/158" />
                </div>
                <div className="case-study__list-item--text">
                  <div className="ad-posted">
                    <img src="http://placekitten.com/29/30" alt="Facebook" />
                  </div>
                  <div className="ad-info">
                    <p className="advertise">2015 4Q GS Shop</p>
                    <p className="ad-type">Facebook DynamicAds</p>
                  </div>
                </div>
              </div>
              <div className="case-study__list-item">
                <div className="case-study__list-item--thumb">
                  <img src="http://placekitten.com/255/158" />
                </div>
                <div className="case-study__list-item--text">
                  <div className="ad-posted">
                    <img src="http://placekitten.com/29/30" alt="Facebook" />
                  </div>
                  <div className="ad-info">
                    <p className="advertise">2015 4Q GS Shop</p>
                    <p className="ad-type">Facebook DynamicAds</p>
                  </div>
                </div>
              </div>
              <div className="case-study__list-item">
                <div className="case-study__list-item--thumb">
                  <img src="http://placekitten.com/255/158" />
                </div>
                <div className="case-study__list-item--text">
                  <div className="ad-posted">
                    <img src="http://placekitten.com/29/30" alt="Facebook" />
                  </div>
                  <div className="ad-info">
                    <p className="advertise">2015 4Q GS Shop</p>
                    <p className="ad-type">Facebook DynamicAds</p>
                  </div>
                </div>
              </div>
              <div className="case-study__list-item">
                <div className="case-study__list-item--thumb">
                  <img src="http://placekitten.com/255/158" />
                </div>
                <div className="case-study__list-item--text">
                  <div className="ad-posted">
                    <img src="http://placekitten.com/29/30" alt="Facebook" />
                  </div>
                  <div className="ad-info">
                    <p className="advertise">2015 4Q GS Shop</p>
                    <p className="ad-type">Facebook DynamicAds</p>
                  </div>
                </div>
              </div>
              <div className="case-study__list-item">
                <div className="case-study__list-item--thumb">
                  <img src="http://placekitten.com/255/158" />
                </div>
                <div className="case-study__list-item--text">
                  <div className="ad-posted">
                    <img src="http://placekitten.com/29/30" alt="Facebook" />
                  </div>
                  <div className="ad-info">
                    <p className="advertise">2015 4Q GS Shop</p>
                    <p className="ad-type">Facebook DynamicAds</p>
                  </div>
                </div>
              </div>
              <div className="case-study__list-item">
                <div className="case-study__list-item--thumb">
                  <img src="http://placekitten.com/255/158" />
                </div>
                <div className="case-study__list-item--text">
                  <div className="ad-posted">
                    <img src="http://placekitten.com/29/30" alt="Facebook" />
                  </div>
                  <div className="ad-info">
                    <p className="advertise">2015 4Q GS Shop</p>
                    <p className="ad-type">Facebook DynamicAds</p>
                  </div>
                </div>
              </div>
              <div className="case-study__list-item">
                <div className="case-study__list-item--thumb">
                  <img src="http://placekitten.com/255/158" />
                </div>
                <div className="case-study__list-item--text">
                  <div className="ad-posted">
                    <img src="http://placekitten.com/29/30" alt="Facebook" />
                  </div>
                  <div className="ad-info">
                    <p className="advertise">2015 4Q GS Shop</p>
                    <p className="ad-type">Facebook DynamicAds</p>
                  </div>
                </div>
              </div>
              <div className="case-study__list-item">
                <div className="case-study__list-item--thumb">
                  <img src="http://placekitten.com/255/158" />
                </div>
                <div className="case-study__list-item--text">
                  <div className="ad-posted">
                    <img src="http://placekitten.com/29/30" alt="Facebook" />
                  </div>
                  <div className="ad-info">
                    <p className="advertise">2015 4Q GS Shop</p>
                    <p className="ad-type">Facebook DynamicAds</p>
                  </div>
                </div>
              </div>
              <div className="case-study__list-item">
                <div className="case-study__list-item--thumb">
                  <img src="http://placekitten.com/255/158" />
                </div>
                <div className="case-study__list-item--text">
                  <div className="ad-posted">
                    <img src="http://placekitten.com/29/30" alt="Facebook" />
                  </div>
                  <div className="ad-info">
                    <p className="advertise">2015 4Q GS Shop</p>
                    <p className="ad-type">Facebook DynamicAds</p>
                  </div>
                </div>
              </div>
              <div className="case-study__list-item">
                <div className="case-study__list-item--thumb">
                  <img src="http://placekitten.com/255/158" />
                </div>
                <div className="case-study__list-item--text">
                  <div className="ad-posted">
                    <img src="http://placekitten.com/29/30" alt="Facebook" />
                  </div>
                  <div className="ad-info">
                    <p className="advertise">2015 4Q GS Shop</p>
                    <p className="ad-type">Facebook DynamicAds</p>
                  </div>
                </div>
              </div>
            </div>
            <Pagination styles={{
              textAlign: 'right',
              marginTop: '10px'
            }}/>
          </div>
          <ResourcesPopup data={blogJson}>
            <div>
              Hello World!
            </div>
          </ResourcesPopup>
        </div>

      </div>
    )
  }
}

// Search Panel

class SearchPanel extends Component {

  render() {
    return (
      <div className="search-box">
        <input className="search-box__input" type="text" name="" id="" placeholder="검색"/>
        <button className="search-box__button" type="button">SEARCH</button>
      </div>
    )
  }
}

class Pagination extends Component {
  constructor(props){
    super(props);
    // https://facebook.github.io/react/tips/inline-styles.html 인라인 스타일 형식
    this.state = {
      styles: props.styles
    }
  }
  render(){
    return (
      <ul className="pagination" style={this.state.styles}>
        <li><a href="#" className="prev">&laquo;</a></li>
        <li><a href="#" className="active">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#" className="next">&raquo;</a></li>
      </ul>
    )
  }
}

Pagination.propTypes = {
  position: React.PropTypes.object
};

class ResourcesPopup extends Component {
  constructor(props){
    super(props);
    this.state = {
      json: this.props.data,
      title: this.props.data.case
    }
  }

  componentDidMount(){
    console.log(this.state.json);

    this.setState({
      title: 'AAAAA'
    })
  }

  render(){
    console.log(this.state.json.case);

    return (
      <div className="modal">
        <div className="modal__container">
          <div className="modal__left">

            <div className="modal__bubble">
              <p>WISE<br/>CASE STUDY</p>
            </div>

          </div>
          <div className="modal__right">
            여기에 텍스트가 들어갑니다!!!!
          </div>
          <a href="#" className="modal__action--close">Close</a>
        </div>
      </div>
    )
  }
}