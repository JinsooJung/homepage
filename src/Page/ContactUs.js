import React, {Component} from 'react';

export default class ContactUs extends Component {
  render() {
    return (
      <div>
        <div>
          {/* 지도 들어갈 부분 -- 시작 */}
          <div>

          </div>
          {/* 지도 들어갈 부분 -- 끝 */}
          <div>
            <p>
              WE KNOW <br/>YOU NEED US!
            </p>
          </div>

        </div>

        <div>
          {/* 테이블 부분 -- 시작 */}
          <div>
            <table>
              <tbody>
              <tr>
                <th>First Name</th>
                <td><input type="text" tabIndex="1"/></td>
                <th>Company Type</th>
                <td>
                  <select name="" id="" tabIndex="6">
                    <option value="">Company Type</option>
                  </select>
                </td>  
              </tr>
              <tr>
                <th>Last Name</th>
                <td>
                  <input type="text" tabIndex="2"/>
                </td>
                <th>Interested Service</th>
                <td>
                  <select name="" id="" tabIndex="7">
                    <option value="">Interested Service</option>
                  </select>
                </td>
              </tr>
              <tr>
                <th>Email</th>
                <td>
                  <input type="text" tabIndex="3"/>
                </td>
                <th>Yearly Budget</th>
                <td>
                  <input type="text" tabIndex="8"/>
                </td>
              </tr>
              <tr>
                <th>Company Name</th>
                <td>
                  <input type="text" tabIndex="4"/>
                </td>
                <th>Your Concern or Goal</th>
                <td>
                  <input type="text" tabIndex="9"/>
                </td>
              </tr>
              <tr>
                <th>Company Region</th>
                <td>
                  <input type="text" tabIndex="5"/>
                </td>
                <td colSpan="2">
                  <input type="checkbox" tabIndex="10"/>
                  <label htmlFor="">I accept the privacy policy</label>
                  <button type="button">SAVE</button>
                </td>

              </tr>
              </tbody>
            </table>
          </div>
          {/* 테이블 부분 -- 끝 */}

          {/* Job Penings -- 시작 */}
          <div>

          </div>
          {/* Job Penings -- 끝 */}
        </div>
        {/* */}
        <div>
          <div>
            <dl>
              <dt>Wisebirds Inc</dt>
              <dd>
                17F, Donghun-Tower, 702-19
              </dd>
            </dl>
          </div>
        </div>

      </div>

    )
  }
}