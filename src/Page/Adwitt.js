import React, { Component } from 'react';
// import Slider from 'react-slick';
import './Adwitt.scss';

class Adwitt extends Component {
  render(){
    return (
      <div className="adwitt">
        <div>
          Adwitt
          Adwitt은 Wisebirds가 제공하는 광고 최적화 플랫폼입니다
        </div>
        <div>
          {/*<SliderAdwitt />*/}
        </div>
      </div>
    )
  }
}

// class SliderAdwitt extends Component {
//   render() {
//     return (
//       <div className="adwitt__slider-layout">
//         <div className="adwitt__slider">
//           <div>
//             <h2>Faster & Easier</h2>
//             <p>빠르고 쉬운  광고 생성  및 운영</p>
//             <div>
//               <h3>Bulk Advert Creation</h3>
//               <p>Adwitt은 사용자의 편의를 극대화한 UI로 Power Editor 대비 쉬운 광고등록이 가능합니다.
//                 특히, 캐로셀 광고 집행 시 다수의 크리에이티브 및 크리에이티브세트를 추가함에 있어
//                 <strong>몇 번의 클릭 만으로 캠페인 세팅 시간 단축에 탁월한 효과를 보여줍니다.</strong> </p>
//             </div>
//             <div>
//               <h3>One-click Ad Set Segmentation</h3>
//               <p>다양한 타겟 그룹을 생성하기 위해 필요했던 반복작업을 <strong>몇 번의 클릭만으로
//                 성별/연령/관심사 등을 기준으로
//                 수 십 개의 애드셋을 한번에 생성하여 광고 등록 시간을 크게 단축할 수 있습니다.</strong></p>
//             </div>
//             <div>
//               <h3>Audience Export from Previous Campaigns</h3>
//               <p><strong>자주 사용하는 타겟 그룹은 따로 저장하여 언제든 불러오기가 가능하며,</strong>
//                 최근 등록한 캠페인과 동일한 타겟그룹을
//                 바 로 선택하여 사용할 수 있습니다.</p>
//             </div>
//           </div>
//           <div>
//             <h2>Closer</h2>
//             <p>광고 효율을 높여줄 정확한 타겟을 발굴</p>
//             <div>
//               <h3>Auto Target Audience Recommendation</h3>
//               <p>페이스북 페이지 분석 데이터와 과거 캠페인 진행 분석을 통해 최고의 효율을 위한 타겟을 추천합니다.
//                 Adwitt은 팬 페이지 분석 데이터와 과거 캠페인 데이터 분석을 통해 캠페인이 도달 될 수 있는 사람, 좋아할 것
//                 같은 사람 그리고 Engage를 할 것 같은 사람을 국가별, 광고타입별, 광고도달별, Demographic 별로 추합니다.
//               </p>
//             </div>
//             <div>
//               <h3>Lookalike Audience Scale-up</h3>
//               <p>광고를 하고자 하는 타겟의 모수가 적을 경우 해당 타겟과 유사한 행동 패턴을 보이는 페이스북 유저를
//                 분석하고 추출하여 광고 모수를 확대하여 일반 Power Editor에 추출하는 Lookalike Audience보다
//                 2배 이상의 모수 확보가 가능합니다.</p>
//             </div>
//             <div>
//               <h3>And-Or-Exclusion Flexible Targeting</h3>
//               <p>Facebook 광고 성과 개선을 위해서는 각 타겟그룹 간의 중복을 제외하여 코어한 타겟을 찾는 것이 중요합니다.
//                 Adwitt에서는 쉽고 직관적인 UI를 기반으로 Interest 및 Behavior에서 And or Exclusion Flexible 타겟팅이 가능합니다.
//                 교집합과 여집합 혹은 그 외에 조합을 통해 원하는 오디언스에 효과적으로 광고를 도달시킬 수 있습니다.
//               </p>
//             </div>
//           </div>
//           <div>
//             <h2>Higher</h2>
//             <p>캠페인 효율제고</p>
//             <div>
//               <h3>Intuitive Grid View</h3>
//               <p>Adwitt의 User Friendly한 UI의 Gride View는 각 Ad Set과 Ad, Creative에 대한 효율을 한 눈에 파악하여 캠페인 상황별 빠른 대응을 가능하게 합니다. 또한 Key Metric을 선택하게 되면, 선택된 Metric의 평균 보다 낮은
//                 Ad 들이 표시되어 각 Ad 들을 수정하거나 중지하여 캠페인 효율을 극대화 시킬 수 있습니다.
//               </p>
//             </div>
//             <div>
//               <h3>Auto Optimization (Ad Set & Ad)</h3>
//               <p>Adwitt의 자동 최적화로 기준이 되는 값을 설정하여 그 보다 낮은 효율의 Ad Set와 Ad들은 자동 종료되어 효율을 극대화 합니다. 이를 위해 Adwitt의 자동 최적화 기능은 기준 값보다 효율이 좋지 않은 AdSet과 Ad들이 설정한 옵션에 맞게 자동 운영되어 KPI 달성이 보다 쉽고편리합니다.
//               </p>
//             </div>
//             <div>
//               <h3>Auto Optimization (Bid)</h3>
//               <p>Facebook 광고는 RTB를 기반으로 경쟁상황에 따라 입찰가를 조정하여 운영하여야 합니다. Adwitt은 경쟁상황에 따라 입찰가를 자동 조정할 수 있어 효과적인 광고 집행이 가능하여
//                 변화화는 입찰 경쟁 하에서 유연하게 대응할 수 있어 사용자들의 수고를 훨씬 덜어 줍니다.
//               </p>
//             </div>
//             <div>
//               <h3>Dashboard : Campaign Status at a Glance</h3>
//               <p>집행되고 있는 캠페인의 추이를 확인하는 것은 현재 상황에 대한 인사이트와 이에 맞는 매니징을 해야 하기 때문에 중요합니다. 이를 위해, Adwitt의 Dashboard에서는 진행되는 캠페인의 실시간 추이를 직관적인 그래프로 확인할 수 있어 효과적인 캠페인 모니터링이 가능합니다.
//               </p>
//             </div>
//           </div>
//           <div>
//             <h2>Stronger</h2>
//             <p>Adwitt이 제공하는 독점 기능들</p>
//             <div>
//               <h3>Dynamic Ads</h3>
//               <p>Dynamic Ads는 웹사이트에서  특정 상품을 조회한 타겟에게  연관된 상품들을 지속적으로 자동 노출시키는 리마켓팅 광고로써, 웹사이트 방문자의 구매 전환을 극대화할 수 있는 Retail, Travel, Commerce 업종에 적합합니다. 리타겟팅 기준으로 상품조회, 장바구니담기, 구매  뿐 아니라 API플랫폼에서만 가능한 검색, 카테고리 조회 값까지 활용이 가능합니다.
//               </p>
//             </div>
//             <div>
//               <h3>Updated WCA</h3>
//               <p>Adwitt의 자동 최적화로 기준이 되는 값을 설정하여 그 보다 낮은 효율의 Ad Set와 Ad들은 자동 종료되어 효율을 극대화 합니다. 이를 위해 Adwitt의 자동 최적화 기능은 기준 값보다 효율이 좋지 않은 Ad Set과 Ad들이 설정한 옵션에 맞게 자동 운영되어 KPI 달성이 보다 쉽고 편리합니다.
//               </p>
//             </div>
//             <div>
//               <h3>Bid Types for API Only</h3>
//               <p>Facebook 과 Instagram은 광고 목적별로 각기 다른 최적화목표 및 과금 기준을 제공하고 있습니다. Power Editor 나 Ad Manager에서 적용 되지 않는 Bid Type도 API 를 통해 제공이 가능합니다. 이를 통해 광고 목적 별로 더욱 다양한 최적화 목표와 과금 기준을 적용할 수 있습니다.
//               </p>
//             </div>
//             <div>
//               <h3>Fast and Easy Reporting</h3>
//               <p>Facebook 광고는 RTB를 기반으로 경쟁상황에 따라 입찰가를 조정하여 운영하여야 합니다.
//                 Adwitt은 경쟁상황에 따라 입찰가를 자동 조정할 수 있어 효과적인 광고 집행이 가능하여변화화는 입찰 경쟁 하에서 유연하게 대응할 수 있어 사용자들의 수고를 훨씬 덜어 줍니다.
//               </p>
//             </div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

export default Adwitt;