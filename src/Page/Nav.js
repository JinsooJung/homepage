import React, { Component } from 'react';
import './Nav.scss';

var dropdownList = [
  {lang: "Korean"},
  {lang: "English"}
];

export class Dropdown extends Component {

  constructor(props){
    super(props);
    this.state = {
      list: props.list,
      selected: props.list[0]
    };
  }

  _onClick(index){
    this.setState({
      selected: this.state.list[index]
    })
  }

  render(){
    var _this = this;
    var renderListItem = this.state.list.map(function(list,index){
      return (
        <div className="dropdown__list-item" key={index} onClick={_this._onClick.bind(_this,index)}>{list.lang}</div>
      )
    });

    return (
      <div className="dropdown">
        <div className="dropdown__display">
          <span>{this.state.selected.lang}</span>
          <i className="arrow">&nbsp;</i>
        </div>
        <div className="dropdown__list">
          {renderListItem}
        </div>
      </div>
    )
  }
}

export default class Nav extends Component {
  render(){
    return (
      <nav className="gnb" id="gnb">
        <ul className="gnb__list">
          <li className="gnb__list-item gnb__list-item--normal"><a href="#">About Wisebirds</a></li>
          <li className="gnb__list-item gnb__list-item--normal"><a href="#">Services</a></li>
          <li className="gnb__list-item gnb__list-item--normal"><a href="#">Adwitt</a></li>
          <li className="gnb__list-item gnb__list-item--logo"><a href="#"><img src={require('./images/gnb__logo.png')}/></a></li>
          <li className="gnb__list-item gnb__list-item--normal"><a href="#">Resources</a></li>
          <li className="gnb__list-item gnb__list-item--normal"><a href="#">Contact Us</a></li>
          <li className="gnb__list-item gnb__list-item--abnormal">
            <Dropdown list={dropdownList}/>
            <a href="#" className="login">Login</a>
          </li>
        </ul>
      </nav>
    )
  }
}

