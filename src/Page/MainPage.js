import React, {Component} from 'react';

import './MainPage.scss';

class MainPage extends Component {
  render() {
    return (
      <div>
        <p className="txt--hello"><img src={require('./images/txt--hello.png')} alt="Hello!"/></p>
        <div className="scroll-down">
          <div className="img animated bounce infinite"><img src={require('./images/img--scroll-down.png')} alt=""/></div>
          <p className="txt">SCROLL DOWN</p>
        </div>
      </div>
    );
  }
}

export default MainPage;